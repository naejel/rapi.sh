# rapi.sh

Get next rers, metros, bus at the stop of your choice in CLI.

Use curl and [Pierre Grimaud API](https://api-ratp.pierre-grimaud.fr/v3/documentation) to get data

Use jq to parse json

# Install

Copy rapi.sh to /bin without the .sh extension
Make it executable 
> sudo chmod +x /bin/rapi 

# Usage

To get next metros from line 4 at denfert rochereau

> rapi metros 4 denfert+rochereau

To get next buses from line 197 at porte d'orleans

> rapi bus 197 porte+orleans

Also work with rers and noctiliens