#!/bin/bash

curl -s https://api-ratp.pierre-grimaud.fr/v3/schedules/$1/$2/$3/A+R?_format=json | jq '.result.schedules[] | .code, .message, .destination'
